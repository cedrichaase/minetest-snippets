if event.type == "program" then
  mem.itemtable = {}
end


if event.type == "item" then
  local itemname = event.item.name
  local count = event.item.count

  if not mem.itemtable[itemname] then
    mem.itemtable[itemname] = 0
  end

  mem.itemtable[itemname] = mem.itemtable[itemname] + count

  digiline_send("storage_in_itemtable", mem.itemtable)
end

return "blue"
