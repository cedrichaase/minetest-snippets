function sum(t)
    local sum = 0
    for k,v in pairs(t) do
        sum = sum + v
    end

    return sum
end

function count(t)
  local count = 0
  for k, v in pairs(t) do
    count = count + 1
  end

  return count
end

function highest_value(t)
  local highest_k = ""
  local highest_v = 0

  for k, v in pairs(t) do
    if v > highest_v then
      highest_k = k
      highest_v = v
    end
  end

  return highest_k
end

function percent(a, b)
  return math.floor((a / b) * 100)
end

if (event.type == "digiline") and (event.channel == "item_tables") then
  local tables = event.msg

  if next(tables) == nil then
    digiline_send("text_sorted", "no data :(\nSend items through \'storage_in\'")
    digiline_send("text_unsorted", "to calculate sorting statistics")
  end

  local storage_fallback = tables["item_storage_fallback"]
  local storage_in       = tables["item_storage_in"]

  -- item count statistics
  local sum_in       = sum(storage_in)
  local count_sorted = sum_in - sum(storage_fallback)

  -- item type statistics
  local itemtypes_in = count(storage_in)
  local itemtypes_sorted = itemtypes_in - count(storage_fallback)

  print(percent(itemtypes_sorted, itemtypes_in) .. "% of item types sorted")
  print(percent(count_sorted, sum_in) .. "% of items sorted")
  digiline_send("text_unsorted", "most unsorted:\n" .. highest_value(storage_fallback):gsub(":", "\n") .. "\n\n(" .. storage_fallback[highest_value(storage_fallback)] .. ")")
  digiline_send("text_sorted", "---sorted---\n" .. percent(count_sorted, sum_in) .. "% of items\n" .. percent(itemtypes_sorted, itemtypes_in) .. "% of types")
end
