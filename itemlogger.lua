local channel = "storage_fallback"

if event.type == "item" then
  digiline_send("item_"..channel, event.item)
end

return "blue"
