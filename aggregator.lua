interrupt(10)

if event.type == "program" then
  mem.tables = {}
end


function string_startswith(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end


if event.type == "digiline" and string_startswith(event.channel, "item") then
  local channel = event.channel
  local item = event.msg

  if not mem.tables[channel] then
    mem.tables[channel] = {}
  end

  if not mem.tables[channel][itemname] then
    mem.tables[channel][item.name] = 0
  end

  mem.tables[channel][item.name] = mem.tables[channel][item.name] + item.count
end


if event.type == "interrupt" then
  digiline_send("item_tables", mem.tables)
end
