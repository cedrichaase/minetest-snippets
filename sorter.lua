output_sorted = "blue"
output_default = "white"

-- The items to output to 'output_sorted'
items = {"default:iron_lump"}


function string_startswith(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end

function string_contains(str, term)
  return string.find(str, term, 0, true)
end

local compare_fn = string_contains

-- Check if an array-like table contains a value
function array_contains(ary, itemstring)
    for index, item in ipairs(ary) do
        if compare_fn(itemstring, item) then
            return true
        end
    end

    return false
end


if event.type == "item" and array_contains(items, event.item.name) then
  return output_sorted
end

return output_default
